const menuTrigger = document.querySelector(".menu-trigger"),
    menu = document.querySelector(".menu"),
    mobileQuery = getComputedStyle(document.body).getPropertyValue("--phoneWidth"),
    isMobile = () => window.matchMedia(mobileQuery).matches,
    isMobileMenu = () => {
        menuTrigger && menuTrigger.classList.toggle("hidden", !isMobile()), menu && menu.classList.toggle("hidden", isMobile());
    };
isMobileMenu(), menuTrigger && menuTrigger.addEventListener("click", () => menu && menu.classList.toggle("hidden")), window.addEventListener("resize", isMobileMenu);